create table usuario (id integer not null, birth_date timestamp, name varchar(255), primary key(id))

insert into usuario values(1, sysdate(), 'AB')
insert into usuario values(2, sysdate(), 'Jil')
insert into usuario values(3, sysdate(), 'Jam')

insert into user values(1001, sysdate(), 'AB')
insert into user values(1002, sysdate(), 'Jil')
insert into user values(1003, sysdate(), 'Jam')

insert into post values(11001, 'My Firts Post', 1001)
insert into post values(11002, 'My Second Post', 1001)
